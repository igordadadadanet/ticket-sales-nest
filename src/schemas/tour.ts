export interface ITour {
  description: string,
  id: string,
  img: string,
  name: string,
  price: string,
  tourOperator: string,
  type: string,
  locationId: string
}

import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";


export type TourDocument = HydratedDocument<ITour>;

@Schema()
export class Tour implements ITour {
  @Prop()
  description: string;

  @Prop()
  id: string;

  @Prop()
  img: string;

  @Prop()
  name: string;

  @Prop()
  price: string;

  @Prop()
  tourOperator: string;

  @Prop()
  type: string;

  @Prop()
  locationId: string;


}

export const TourSchema = SchemaFactory.createForClass(Tour);