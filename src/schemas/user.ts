import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { IUser } from "../interfaces/user-interface";

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User implements IUser {
  @Prop()
  login: string;

  @Prop()
  password: string;

  @Prop()
  email: string;

  @Prop({required: false})
  cardNumber: number;

  @Prop({required: false})
  _id: string;
}

export const UserSchema = SchemaFactory.createForClass(User);