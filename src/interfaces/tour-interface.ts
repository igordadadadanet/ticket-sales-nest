export interface ITour {
  description: string,
  id: string,
  img: string,
  name: string,
  price: string,
  tourOperator: string,
  type: string,
  locationId: string
}

export interface ITourClient {
  name: string,
  description: string,
  operator?: string,
  price?: string,
  img?: any
}