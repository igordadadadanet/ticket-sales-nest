export interface IUser {
  login: string,
  email: string,
  password: string;
  cardNumber?: number;
  _id: any;
}