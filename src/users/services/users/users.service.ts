import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {User, UserDocument} from "../../../schemas/user";
import { Model, Schema } from "mongoose";
import {UserDto} from "../../../dto/user-dto";
import {JwtService} from "@nestjs/jwt";
import { IUser } from "../../../interfaces/user-interface";
import { Types } from "mongoose";


@Injectable()
export class UsersService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>, private jwtService: JwtService) {
    }

    async getAllUsers(): Promise<User[]> {
        return this.userModel.find();
    }

    async getUserById(id: string): Promise<User> {
        return this.userModel.findById(id);
    }

    async sendUser(data: IUser): Promise<User> {
        data._id = new Types.ObjectId()
        const userData = new this.userModel(data);
        return userData.save();
    }

    async updateUser(id: string, body: UserDto): Promise<User> {
        return this.userModel.findByIdAndUpdate(id, body);
    }

    async deleteUserById(id: string): Promise<User> {
        return this.userModel.findByIdAndRemove(id);
    }

    async deleteUsers(): Promise<User> {
        return this.userModel.remove();
    }

    async checkUser(login): Promise<User[]> {
        return this.userModel.find({login: login});
    }

    async checkAuthUser(login, password): Promise<User[]> {
        const userArr = await this.userModel.find({login, password});
        return userArr.length ? userArr : null;
    }


    async login(user: UserDto) {
        const payload = {login: user.login, password: user.password};
        const userFromDb = await this.userModel.find({login: user.login})
        return {
            id: userFromDb[0]._id,
            access_token: this.jwtService.sign(payload)
        }
    }
}
