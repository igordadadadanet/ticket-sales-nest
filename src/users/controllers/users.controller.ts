import {Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseGuards} from "@nestjs/common";
import { UsersService } from '../services/users/users.service';
import { User } from "../../schemas/user";
import { UserDto } from "../../dto/user-dto";
import {AuthGuard} from "@nestjs/passport";
import {JwtAuthGuardService} from "../../services/authentitication/jwt-auth-guard/jwt-auth-guard.service";
import mongoose from "mongoose";

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {
  }

  @Get()
  getAllUsers(): Promise<User[]> {
    return this.usersService.getAllUsers();
  }

  @Get(':id')
  getUserById(@Param('id') id): Promise<User> {
    return this.usersService.getUserById(id);
  }

  // @UseGuards(JwtAuthGuardService)
  @Post()
  sendUser(@Body() data: UserDto): Promise<User> {
    console.log(data);
    return this.usersService.checkUser(data.login)
      .then((queryRes: User[]) => {
        console.log('registration', queryRes);
        if (queryRes.length === 0) {
          return this.usersService.sendUser(data);
        } else {
          console.log('user is exist');
          throw new HttpException({
            status: HttpStatus.CONFLICT,
            errorText: 'Пользователь уже зарегестрирован'
          }, HttpStatus.CONFLICT);
        }
      })
  }

  @UseGuards(AuthGuard('local'))
  @Post(':login')
  auth(@Body() data: UserDto) {
    return this.usersService.login(data);
  }

  @Put(':id')
  updateUser(@Param('id') id: string, @Body() body: UserDto): Promise<User> {
    return this.usersService.updateUser(id, body);
  }

  @Delete()
  deleteUsers(): Promise<User> {
    return this.usersService.deleteUsers();
  }

  @Delete(':id')
  deleteUserById(@Param('id') id): Promise<User> {
    return this.usersService.deleteUserById(id);
  }
}
