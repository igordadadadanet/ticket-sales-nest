import {ITour} from "../interfaces/tour-interface";

export class TourDto implements ITour {
    description: string;
    id: string;
    img: string;
    name: string;
    price: string;
    tourOperator: string;
    type: string;
    locationId: string;
    constructor(name, description, tourOperator, price, img?) {
        this.name = name;
        this.description = description;
        this.tourOperator = tourOperator;
        this.price = price;
        this.img = img;
    }

}