import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Tour, TourDocument} from "../../schemas/tour";
import {TourDto} from "../../dto/tour-dto";
import {User} from "../../schemas/user";
import { ITour, ITourClient } from "../../interfaces/tour-interface";


@Injectable()
export class ToursService {
    private toursCount = 10;

    constructor(@InjectModel(Tour.name) private tourModel: Model<TourDocument>) {
    }

    async getAllTours(): Promise<Tour[]> {
        return this.tourModel.find();
    }

    async getTourById(id: string): Promise<ITour> {
        return this.tourModel.findById(id);
    }

    async getToursByName(name: string): Promise<ITour[]> {
        return this.tourModel.find({ "name" : { $regex: name, $options: 'i' }});
    }

    async generateTours(): Promise<Tour[]> {
        for (let i = 0; i <= this.toursCount; i++) {
            const tour = new TourDto(`name:${i}`, `description:${i}`, `operator:${i}`, i);
            const tourData = new this.tourModel(tour);
            await tourData.save();
        }
        return this.getAllTours();
    }

    async deleteTours(): Promise<Tour[]> {
        await this.tourModel.remove();
        return this.tourModel.find();
    }

    async uploadTour(body: ITourClient) {
        const tour = new TourDto(body.name, body.description, body.operator, body.price, body.img);
        const tourData = new this.tourModel(tour);
        await tourData.save();
    }
}
