import { Module } from "@nestjs/common";
import { ToursController } from "./controllers/tours/tours.controller";
import { ToursService } from "./services/tours.service";
import { MongooseModule } from "@nestjs/mongoose";
import { Tour, TourSchema } from "../schemas/tour";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { jwtConstants } from "../static/private/constants";
import { JwtStrategyService } from "../services/authentitication/jwt-strategy/jwt-strategy.service";
import { TourItemController } from "./controllers/tour-item/tour-item.controller";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Tour.name, schema: TourSchema }]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret
    })
  ],
  controllers: [
    ToursController,
    TourItemController
  ],
  providers: [
    ToursService,
    JwtStrategyService
  ]
})
export class ToursModule {
}
