import {Controller, Delete, Get, Param, Post, UseGuards} from "@nestjs/common";
import {ToursService} from "../../services/tours.service";
import {JwtAuthGuardService} from "../../../services/authentitication/jwt-auth-guard/jwt-auth-guard.service";
import {ITour} from "../../../interfaces/tour-interface";
import {Tour} from "../../../schemas/tour";
import {User} from "../../../schemas/user";

@Controller('tours')
export class ToursController {
    constructor(private toursService: ToursService) {
    }

    @UseGuards(JwtAuthGuardService)
    @Get()
    getAllTours(): Promise<ITour[]> {
        console.log('get all tours')
        return this.toursService.getAllTours();
    }

    @Get(':id')
    getTourById(@Param('id') id): Promise<ITour> {
        console.log(id)
        return this.toursService.getTourById(id);
    }

    @UseGuards(JwtAuthGuardService)
    @Post()
    initTours(): Promise<Tour[]> {
        return this.toursService.generateTours();
    }

    @UseGuards(JwtAuthGuardService)
    @Delete()
    removeAllTours(): Promise<Tour[]> {
        return this.toursService.deleteTours();
    }
}
