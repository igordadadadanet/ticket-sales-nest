import { Body, Controller, Get, Post } from "@nestjs/common";
import {OrderService} from "../services/order.service";
import {OrderDto} from "../../dto/order-dto";
import {Order} from "../../schemas/order";


@Controller('order')
export class OrderController {
    constructor(private orderService: OrderService) {
    }

    @Post()
    initOrder(@Body() data: OrderDto): Promise<Order> {
        console.log(data)
        const orderData = new OrderDto(data.age, data.birthDay, data.cardNumber, data.tourId, data.userId);
        return this.orderService.sendOrder(orderData);
    }

    @Get()
    getOrders(): Promise<Order[]> {
        return this.orderService.getOrders();
    }
}
