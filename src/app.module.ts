import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UsersModule } from "./users/users.module";
import { MongooseModule } from "@nestjs/mongoose";
import { ToursModule } from './tours/tours.module';
import { OrderModule } from './order/order.module';

@Module({
  imports: [
    UsersModule,
    MongooseModule.forRoot('mongodb://localhost:27017/nest'),
    ToursModule,
    OrderModule
  ],
  controllers: [AppController],
  providers: [
    AppService
  ]
})
export class AppModule {
}
